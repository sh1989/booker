import React from 'react';
import './App.css';
import SelectRoster from './components/SelectRoster';
import { generateRoster } from './model/generateRoster';
import logo from './logo.png';

const roster = generateRoster();

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} alt="Logo" />
        <h1>Booker</h1>
      </header>
      <main>
        <SelectRoster wrestlers={roster} />
      </main>
    </div>
  );
}

export default App;
