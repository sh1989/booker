import React, { FunctionComponent} from 'react'
import Wrestler from '../model/Wrestler'
import './SelectRoster.css'

interface ProgressBarProps {
  value: number
}

const ProgressBar: FunctionComponent<ProgressBarProps> = ({ value }) => {
  let className = 'average'
  if (value > 12) {
    className = 'exceptional'
  }
  if (value < 8) {
    className = 'mediocre'
  }

  return (
    <>
      {value} <progress className={className} max={20} value={value} />
    </>
  )
}

interface SelectRosterProps {
  wrestlers: Wrestler[]
}

const SelectRoster : FunctionComponent<SelectRosterProps> = ({ wrestlers }) => (
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Strength</th>
        <th>Agility</th>
        <th>Stamina</th>
        <th>Charisma</th>
        <th>Cost</th>
        <th>Sign?</th>
      </tr>
    </thead>
    <tbody>
      {wrestlers.map((w) => (
        <tr key={w.id}>
          <td>{w.name}</td>
          <td><ProgressBar value={w.strength} /></td>
          <td><ProgressBar value={w.agility} /></td>
          <td><ProgressBar value={w.stamina} /></td>
          <td><ProgressBar value={w.charisma} /></td>
          <td><ProgressBar value={w.strength} /></td>
          <td>{w.signingFee}</td>
          <td><button>Sign</button></td>
        </tr>
      ))}
    </tbody>
  </table>
)

export default SelectRoster
