export default class Wrestler {
  readonly id : number

  constructor(
    readonly name : string,
    readonly strength : number,
    readonly agility : number,
    readonly stamina : number,
    readonly charisma : number,
    readonly signingFee: number
  ) {
    this.id = Math.random()
  }
}
