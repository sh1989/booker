import Wrestler from './Wrestler';

const names = [
  "Vengeance",
  "The Sweeper",
  "Hardcore Harry",
  "Moxie",
  "The Reverend",
  "Wolf",
  "All-Star",
  "Archfiend",
  "Jack",
  "C.W.O"
];

export function generateRoster() : Wrestler[] {
  return names.map(n => {
    const strength = randomInclusive(5, 15);
    const agility = (20 - strength) + randomInclusive(0, 2);
    const stamina = randomInclusive(5, 15);
    const charisma = randomInclusive(5, 15);
    const signingFee = strength + agility + stamina + charisma;

    return new Wrestler(n, strength, agility, stamina, charisma, signingFee);
  });
}

function randomInclusive(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
