import { getStages } from './stages/stages';
import Stage from './stages/Stage';

export default class Game {
  readonly stages: Stage[];

  constructor() {
    this.stages = getStages();
  }

  start() {
  }
}
